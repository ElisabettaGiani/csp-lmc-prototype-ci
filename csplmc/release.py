# -*- coding: utf-8 -*-
#
# This file is part of the CentralNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""Release information for Python Package"""

name = """tangods-cspmaster"""
version = "0.1.4"
version_info = version.split(".")
description = """SKA Csp Master TANGO Device Server"""
author = "E.G"
author_email = "elisabetta.giani@inaf.it"
license = """BSD-3-Clause"""
url = """www.tango-controls.org"""
copyright = """"""
